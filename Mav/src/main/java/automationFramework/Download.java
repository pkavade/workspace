package automationFramework;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Download {

	WebDriver driver;
	JavascriptExecutor jse;


	public void invokeBrowser(int fileno) {

		try {
			System.setProperty("webdriver.chrome.driver", "D:/chromedriver_win32/chromedriver.exe");
			String downloadFilepath = "D:\\Moveit";
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilepath);
			chromePrefs.put("safebrowsing.enabled", "true");
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			cap.setCapability(ChromeOptions.CAPABILITY, options);

			driver = new ChromeDriver(cap);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

			driver.get("https://www.moveit.royalmailgroup.com");
			loginmt(fileno);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public void loginmt(int fileno) {
		try {
			
			File src = new File("D:\\log.xlsx");
			FileInputStream fis = new FileInputStream(src);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sh1 = wb.getSheetAt(0);
			final String User = sh1.getRow(0).getCell(0).getStringCellValue();
			final String Pass = sh1.getRow(1).getCell(0).getStringCellValue();
			driver.findElement(By.id("form_username")).sendKeys(User);
			driver.findElement(By.id("form_password")).sendKeys(Pass);
			driver.findElement(By.xpath("//*/span")).click();
			driver.findElement(By.xpath("//*[@id=\"field_gotofolder\"]/option[4]")).click();// go
																							// to
																							// my
																							// folder
//			driver.findElement(By.linkText("IFTMIN error.txt")).click(); // select
//																						// file
//																						// by
//																						// filename
//			driver.findElement(By.xpath("//*[@id=\"content\"]/tbody/tr[7]/td[2]/a[1]")).click();// download
																								// button
			
			 driver.findElement(By.linkText("Created")).click(); //(for
			// created link)
			
				for (int i = 3; i < fileno+3; i++){
					try {
				 driver.findElement(By.xpath("//*[@id=\"folderfilelisttable\"]/tbody/tr["+i+"]/td[13]/a[2]/span")).click();//recent
				 driver.navigate().refresh();
					} catch (NoSuchElementException ex) {
				        //Do nothing
				    }
//			 driver.findElement(By.xpath("//*[@id=\"folderfilelisttable\"]/tbody/tr[4]/td[13]/a[2]/span")).click();// upload
				} 

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		
		 Scanner in = new Scanner(System.in); 
	       System.out.printf("Enter number of files you want to download:  ");
	       int fileno = in.nextInt();

		Download myObj = new Download();
		myObj.invokeBrowser(fileno);

	}

	public void invokeBrowser() {
		// TODO Auto-generated method stub
		
	}

}
